import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { JspadModule } from './jspad/jspad.module';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    JspadModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
