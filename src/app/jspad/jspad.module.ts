import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CodemirrorComponent } from './codemirror/codemirror.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { AngularSplitModule } from '../vendor/angular-split/modules/angularSplit.module';
import { AngularFireModule } from '@angular/fire';

import { AngularFirestore } from '@angular/fire/firestore';

import { NgbDropdownModule } from '@ng-bootstrap/ng-bootstrap';
import { HttpClientModule } from '@angular/common/http';
import { environment } from '../../environments/environment';

@NgModule({
  imports: [
    CommonModule,
    AngularSplitModule,
    NgbDropdownModule,
    HttpClientModule,
    AngularFireModule.initializeApp(environment.firebase)
  ],
  providers: [AngularFirestore],
  declarations: [DashboardComponent, CodemirrorComponent],
  exports: [DashboardComponent]
})
export class JspadModule { }
