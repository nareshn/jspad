import { TestBed } from '@angular/core/testing';

import { PadFactoryService } from './pad-factory.service';

describe('PadFactoryService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PadFactoryService = TestBed.get(PadFactoryService);
    expect(service).toBeTruthy();
  });
});
