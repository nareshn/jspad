// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,

  firebase: {
    apiKey: "AIzaSyCX_EGaxV6NrX-qiiOS2KssEolkGEfu3JI",
    authDomain: "pseudopad-98d1d.firebaseapp.com",
    databaseURL: "https://pseudopad-98d1d.firebaseio.com",
    projectId: "pseudopad-98d1d",
    storageBucket: "pseudopad-98d1d.appspot.com",
    messagingSenderId: "373126360613"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
